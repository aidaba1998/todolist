import React from 'react';
import './App.css';
import Form from './Component/Form';
import Header from './Component/Header';
import  {useState} from 'react';
import Todolist from './Component/Todolist';


function App() {
  const [input,setInput]=useState("");
  const [todos,setTodos]=useState([]);
  const[editTodo,setEditTodo]=useState(null);
  return (
    <div className='container'>
    <div className='app-wrapper'>
     <div>
     <Header  ></Header>
     </div>
     <div>
       <Form input={input} 
       setInput={setInput}
        todos={todos}  
        setTodos={setTodos}
        editTodo={editTodo}
        setEditTodo={setEditTodo}
        ></Form>
     </div>
     <Todolist todos={todos} 
     setTodos={setTodos}
     setEditTodo={setEditTodo}
     ></Todolist>
    </div>
    

    </div>
  )
}

export default App;
